<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Car extends Model{
    protected $table = 'car';
 
    protected $fillable = ['car_name', 'car_nmber', 'driver_id'];	

    public $timestamps = false;

}