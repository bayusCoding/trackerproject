<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Movement;

class MovementController extends Controller{

    public function getLocation($id){
        // Get first 10 rows 'Later'//
        $result = DB::table('movement')
            ->select('lat', 'lng')
            ->where('driver_id', $id)
            ->get();
            
        return response()->json(['result' => $result], 200);
    }
}