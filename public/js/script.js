$(document).ready(function(){
    var map;
    var id = document.getElementById('getId').dataset.id;
    var coordinates = [];

    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 0, lng: -180},
            zoom: 6,
            mapTypeId: 'terrain'
        });

        getCoordinates();
    }

    function createTrackingLine(data){
        coordinates = data.result;
        //console.log(coordinates);

        var flightPath = new google.maps.Polyline({
            path: coordinates,
            geodesic: true,
            strokeColor: 'red',
            strokeOpacity: 1.0,
            strokeWeight: 2
        });

        flightPath.setMap(map);
    }

    function getCoordinates(){
        $.ajax({
            type:'GET',
            url:'/location/'+id,
            success:function(data){
                createTrackingLine(data);
            }
        });
    }

    window.onload = initMap;
})
